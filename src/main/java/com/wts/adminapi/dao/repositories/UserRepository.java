package com.wts.adminapi.dao.repositories;

import com.wts.adminapi.dao.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 * Created by nvtien on 10/4/18.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    @Query(value = "SELECT * FROM users WHERE user_name=?1 AND password=?2", nativeQuery = true)
    User findByUserNameAndPassword(String userName, String password);
}
