package com.wts.adminapi.services;

import com.wts.adminapi.dto.UserDTO;

/**
 * Created by nvtien on 10/4/18.
 */
public interface UserService {
    UserDTO login(UserDTO userDTO);
}
