package com.wts.adminapi.services;

import com.wts.adminapi.dao.models.User;
import com.wts.adminapi.dao.repositories.UserRepository;
import com.wts.adminapi.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by nvtien on 10/4/18.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDTO login(UserDTO userDTO) {
        User user = userRepository.findByUserNameAndPassword(userDTO.getUserName(), userDTO.getPassword());
        if (user != null) {
            userDTO.setUserId(user.getId());
        }
        return userDTO;
    }
}
