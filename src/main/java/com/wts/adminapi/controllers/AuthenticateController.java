package com.wts.adminapi.controllers;

import com.wts.adminapi.dto.Response;
import com.wts.adminapi.dto.UserDTO;
import com.wts.adminapi.services.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by nvtien on 10/4/18.
 */
@RestController
public class AuthenticateController {
    private UserService userService;

    public AuthenticateController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/login")
    public Response<UserDTO> login(@RequestBody UserDTO userDTO) {
        userDTO = userService.login(userDTO);
        if (userDTO.getUserId() != null) {
            return new Response<>(true, userDTO, "");
        } else {
            return new Response<>(false, null, "User not exist");
        }
    }
}
