package com.wts.adminapi.controllers;

import com.wts.adminapi.dto.Response;
import com.wts.adminapi.dto.UserDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nvtien on 10/2/18.
 */
@RestController
public class UserController {
    @GetMapping(value = "/users")
    public Response<List<UserDTO>> getUsers() {
        List<UserDTO> users = new ArrayList<>();
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(1L);
        userDTO.setUserName("admin");
        users.add(userDTO);

        UserDTO user2DTO = new UserDTO();
        user2DTO.setUserId(2L);
        user2DTO.setUserName("user");
        users.add(user2DTO);
        return new Response<>(true, users, "");
    }
}
